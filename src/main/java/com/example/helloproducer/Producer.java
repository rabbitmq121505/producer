package com.example.helloproducer;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class Producer implements CommandLineRunner {

    private final RabbitTemplate rabbitTemplate;

    @Override
    public void run(String... args) throws Exception {
        log.info("Publishing messages to Rabbit");
        int count = 0;
        while(true){
            rabbitTemplate.convertAndSend(HelloProducerApplication.topicExchangeName, "foo.bar.baz", ++count+".Hello from RabbitMQ!");
            Thread.sleep(3000);
        }
    }

}
